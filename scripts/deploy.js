const hre = require("hardhat");

async function main() {
  // Get the ContractFactory for the Counter contract
  const Counter = await hre.ethers.getContractFactory("Counter");

  // Deploy the Counter contract
  const counter = await Counter.deploy();

  // Wait for the contract deployment to be mined
  await counter.deployed();

  // Log the contract address
  console.log("Contract deployed to:", counter.address);

  // Get the deployment transaction receipt
  const txReceipt = await counter.deployTransaction.wait();
  console.log("Deployed by address:", txReceipt.from);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });