// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.24;

contract Counter {

  uint public count;

  function get() public view returns(uint) {
    return count;
  }

  function increment() public {
    count += 1;
  }

  function decrement() public {
    require(count > 0, "underflow");
    count -= 1;
  }
}