import { useState } from "react";
import { ethers } from 'ethers';
require('dotenv').config();
import CounterContract from '../artifacts/contracts/Counter.sol/Counter.json';

const contractAddress = "0x5FbDB2315678afecb367f032d93F642f64180aa3";
const abi = CounterContract.abi;

export default function App() {
  const [connectButton, setConnectButton] = useState("Connect");
  const [count, setCount] = useState("");

  const handleConnect = async () => {
    if (typeof window.ethereum !== 'undefined') {
      try {
        // Request account access
        await window.ethereum.request({ method: "eth_requestAccounts" });
        setConnectButton("Connected");
      } catch (error) {
        console.error("User rejected the request or there was an error", error);
      }
    } else {
      setConnectButton("Please install MetaMask");
    }
  };

  const getCount = async () => {
    if(typeof window.ethereum != 'undefined') {
      //connnect to the blockchain using providers
      const provider = new ethers.providers.Web3Provider(window.ethereum);
      //get interacting user account/signer - current Metamask account
      const signer = await provider.getSigner();
      //interacting with contract abi and address
      const contract = new ethers.Contract(contractAddress, abi, provider);
  
      try {
        const data = await contract.get();
        setCount(data.toString());
      } catch (error) {
        console.log(error)
      }
  
    }
  }

  const handleIncrement = async () => {
    if(typeof window.ethereum != 'undefined') {
      // Connect to the blockchain using providers
      const provider = new ethers.providers.Web3Provider(window.ethereum);
      // Get interacting user account/signer - current Metamask account
      const signer = await provider.getSigner();
      // Interacting with contract ABI and address
      const contract = new ethers.Contract(contractAddress, abi, signer); // Use signer instead of provider
  
      try {
        const tx = await contract.increment({ gasLimit: 1000000 });
        await tx.wait(); // Wait for the transaction to be mined
        // After successful increment, update the count display
        getCount();
      } catch (error) {
        console.log(error);
      }
    }
  }

  const handleDecrement = async () => {
    if(typeof window.ethereum != 'undefined') {
      // Connect to the blockchain using providers
      const provider = new ethers.providers.Web3Provider(window.ethereum);
      // Get interacting user account/signer - current Metamask account
      const signer = await provider.getSigner();
      // Interacting with contract ABI and address
      const contract = new ethers.Contract(contractAddress, abi, signer); // Use signer instead of provider
  
      try {
        // Call the increment function on the contract
        const tx = await contract.decrement({ gasLimit: 1000000 });
        //wait for the transaction to be mined
        await tx.wait();
        // After successful increment, update the count display
        getCount();
      } catch (error) {
        console.log(error);
      }
    }
  }

  return(
    <didv>
       <button onClick={handleConnect}>{connectButton}</button>
       <button onClick={handleIncrement}>Increment Count</button>
       <button onClick={handleDecrement}>Decrement Count</button>
       <button onClick={getCount}>Get Count</button>
       <hr></hr>
       <h1> Count: {count}</h1>
    </didv>
  )
}