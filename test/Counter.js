const { expect } = require('chai');
const { ethers } = require('hardhat');

describe("Counter", function() {

  //declare the counter variable
  let counter;

  //initialise the Counter contract before each test
  this.beforeEach(async function() {
    const Counter = await ethers.getContractFactory("Counter");
    counter = await Counter.deploy();
    await counter.deploymentTransaction().wait();
  })

  //Assert the contract initial count is zero
  it("Should have an initial count of 0", async function() {
    expect(await counter.getCount()).to.equal(0);
  });

  //Assert the contract increments by 1
  it("Should increment by 1", async function() {
    await counter.increment();
    expect(await counter.getCount()).to.equal(1);
  });

  //Assert the contract decrements by 1
  it("Should decrement by 1", async function() {
    await counter.increment();
    await counter.decrement();
    expect(await counter.getCount()).to.equal(0);
  });

  //Assert contract can not decrement below 0
  it("Should not decrement below 0", async function() {
    await expect(counter.decrement()).to.be.revertedWith('underflow');
  });
})